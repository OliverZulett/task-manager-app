import { BrowserRouter, Switch, Route } from "react-router-dom";
import ProjectState from "./context/projects/projectState";
import Login from "./pages/login/Login";
import NewAccount from "./pages/newAccount/NewAccount";
import Projects from "./pages/projects/Projects";
import TaskState from "./context/tasks/taskState";
import AlertState from "./context/alerts/alertState";
import AuthState from "./context/authentication/authState";
import authToken from "./config/token";
import PrivateRoute from "./components/routes/privateRoute";

const token = localStorage.getItem('token');
if (token) {
  authToken(token);
}

function App() {
  return (
    <ProjectState>
      <TaskState>
        <AlertState>
          <AuthState>
            <BrowserRouter>
              <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/new-account" component={NewAccount} />
                <PrivateRoute exact path="/projects" component={Projects} />
              </Switch>
            </BrowserRouter>
          </AuthState>
        </AlertState>
      </TaskState>
    </ProjectState>
  );
}

export default App;
