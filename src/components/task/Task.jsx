import React, { useCallback, useContext } from "react";
import TaskContext from "../../context/tasks/taskContext";

const Task = ({ task }) => {
  const { name, state, _id, project } = task;

  const taskContext = useContext(TaskContext);
  const { removeTask, getTasks, updateTask, selectTask, cleanSelectedTask } = taskContext;

  const onRemoveTask = useCallback(() => {
    removeTask(project, _id);
    getTasks(project);
  }, [_id, project]);

  const onChangeState = useCallback(() => {
    if (task.state) {
      task.state = false;
    } else {
      task.state = true;
    }
    updateTask(task);
    // cleanSelectedTask();
  }, [task]);

  const onSelectTask = useCallback(() => {
    selectTask(task);
  }, [task]);

  return (
    <li className="tarea sombra">
      <p>{name}</p>
      <div className="estado">
        {state ? (
          <button onClick={onChangeState} type="button" className="completo">
            Complete
          </button>
        ) : (
          <button onClick={onChangeState} type="button" className="incompleto">
            Incomplete
          </button>
        )}
      </div>
      <div className="acciones">
        <button
          onClick={onSelectTask}
          type="button"
          className="btn btn-primario"
        >
          Edit
        </button>
        <button
          type="button"
          className="btn btn-secundario"
          onClick={onRemoveTask}
        >
          Delete
        </button>
      </div>
    </li>
  );
};

export default Task;
