import { useContext, useEffect } from "react";
import ProjectContext from "../../context/projects/projectContext";
import TaskContext from "../../context/tasks/taskContext";
import AuthContext from "../../context/authentication/authContext";

const Project = ({ project }) => {

  const authContext = useContext(AuthContext);
  const { getUserAuthenticated } = authContext;

  useEffect(() => {
    getUserAuthenticated();
  }, []);

  const projectsContext = useContext(ProjectContext);
  const { showSelectedProject } = projectsContext;

  const taskContext = useContext(TaskContext);
  const { getTasks } = taskContext;

  const selectProject = project => {
    showSelectedProject(project);
    getTasks(project._id);
  };
  return (
    <li>
      <button
        type="button"
        className="btn btn-blank"
        onClick={() => selectProject(project)}
      >
        {project.name}
      </button>
    </li>
  );
};

export default Project;
