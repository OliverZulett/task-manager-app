import React, { useContext, useEffect } from "react";
import { Redirect, Route } from "react-router-dom";
import AuthContext from "../../context/authentication/authContext";

const PrivateRoute = ({ component: Component, ...props }) => {
  const authContext = useContext(AuthContext);
  const { authenticated, getUserAuthenticated, loading } = authContext;
  useEffect(() => {
    getUserAuthenticated();
  }, []);
  return (
    <Route
      {...props}
      render={(props) =>
        (!authenticated && !loading && <Redirect to="/" />) || <Component {...props} />
      }
    />
  );
};

export default PrivateRoute;
