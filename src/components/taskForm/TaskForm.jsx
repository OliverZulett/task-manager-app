import { useContext, useEffect, useState } from "react";
import ProjectContext from "../../context/projects/projectContext";
import TaskContext from "../../context/tasks/taskContext";

const TaskForm = () => {
  const projectsContext = useContext(ProjectContext);
  const { project } = projectsContext;

  const taskContext = useContext(TaskContext);
  const { taskError, addTask, validateTask, getTasks, selectedTask, updateTask, cleanSelectedTask } = taskContext;

  useEffect(() => {
    if (selectedTask !== null) {
      setTask(selectedTask)
    } else {
      setTask({
        name: ''
      })
    }
  }, [selectedTask]);

  const [task, setTask] = useState({
    name: ""
  });

  const { name } = task;

  if (!project) {
    return null;
  }

  const [selectedProject] = project;

  const handleChange = e => {
    setTask({
      ...task,
      [e.target.name]: e.target.value
    });
  };

  const onSubmitTask = e => {
    e.preventDefault();
    if (name.trim() === "") {
      validateTask();
      return;
    }
    if (selectedTask === null) {
      task.project = selectedProject._id;
      addTask(task);
    } else {
      updateTask(task);
      cleanSelectedTask();
    }
    getTasks(selectedProject._id);
    setTask({
      name: ""
    });
  };

  return (
    <div className="formulario">
      <form onSubmit={onSubmitTask}>
        <div className="contenedor-input">
          <input
            type="text"
            className="input-text"
            placeholder="Task Name"
            name="name"
            value={name}
            onChange={handleChange}
          />
        </div>
        <div className="contenedor-input">
          <input
            type="submit"
            className="btn btn-primario btn-submit btn-block"
            value={selectedTask ? "Edit task" : "Add Task"}
          />
        </div>
      </form>
      {taskError && <p className="mensaje error">Task name is require</p>}
    </div>
  );
};

export default TaskForm;
