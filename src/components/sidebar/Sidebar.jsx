import NewProject from "../../pages/project/NewProject";
import ProjectList from "../../pages/projectList/ProjectList";

const Sidebar = () => {
  return (
    <aside>
      <h1>
        Task
        <span>Manager</span>
      </h1>
      <NewProject />
      <div className="proyectos">
        <h2>Your Projects</h2>
        <ProjectList />
      </div>
    </aside>
  );
};

export default Sidebar;
