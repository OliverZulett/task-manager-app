import { useContext, useEffect } from "react";
import AuthContext from "../../context/authentication/authContext";

const Header = () => {
  const authContext = useContext(AuthContext);
  const { user, getUserAuthenticated, closeSession } = authContext;
  useEffect(() => {
    getUserAuthenticated();
  }, []);
  return (
    <header className="app-header">
      {
        user && 
        <p className="nombre-usuario">
          Hi! <span>{user.name}</span>
        </p>
      }
      <nav className="nav-principal">
        <button
          className="btn btn-blank nombre-usuario"
          onClick={() => closeSession()}
        >
          Close Session
        </button>
      </nav>
    </header>
  );
};

export default Header;
