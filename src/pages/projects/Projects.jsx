import Header from "../../components/header/header";
import Sidebar from "../../components/sidebar/Sidebar";
import TaskForm from "../../components/taskForm/TaskForm";
import TaskList from "../taskList/TaskList";

const Projects = () => {
  return (
    <div className="contenedor-app">
      <Sidebar></Sidebar>
      <div className="seccion-principal">
        <Header />
        <main>
          <TaskForm />
          <div className="contenedor-tareas">
            <TaskList />
          </div>
        </main>
      </div>
    </div>
  );
};

export default Projects;
