import { Fragment, useContext, useState } from "react";
import projectContext from "../../context/projects/projectContext";

const NewProject = () => {
  const projectsContext = useContext(projectContext);
  const { form, errorForm, showForm, addProject, showError } = projectsContext;

  const [project, saveProject] = useState({
    name: ""
  });

  const { name } = project;

  const onChangeProject = event => {
    saveProject({
      ...project,
      [event.target.name]: event.target.value
    });
  };

  const onSubmitProject = event => {
    event.preventDefault();
    if (name === "") {
      showError(true);
      return;
    }
    addProject(project);
    saveProject({
      name: ""
    });
  };
  return (
    <Fragment>
      <button
        type="button"
        className="btn btn-block btn-primario"
        onClick={() => showForm()}
      >
        New Project
      </button>
      {form && (
        <form className="formulario-nuevo-proyecto" onSubmit={onSubmitProject}>
          <input
            type="text"
            className="input-text"
            placeholder="Project Name"
            name="name"
            onChange={onChangeProject}
            value={name}
          />
          <input
            type="submit"
            className="btn btn-primario btn-block"
            value="Add Project"
          />
        </form>
      )}
      {errorForm && <p className="mensaje error">Project nameis require</p>}
    </Fragment>
  );
};

export default NewProject;
