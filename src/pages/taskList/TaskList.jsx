import { Fragment, useContext } from "react";
import Task from "../../components/task/Task";
import ProjectContext from "../../context/projects/projectContext";
import TaskContext from "../../context/tasks/taskContext";
import {
  Transition,
  CSSTransition,
  TransitionGroup
} from "react-transition-group";

const TaskList = () => {
  const projectsContext = useContext(ProjectContext);
  const { project, removeProject } = projectsContext;

  const taskContext = useContext(TaskContext);
  const { projectTasks } = taskContext;

  if (!project) {
    return <h2>Select a project</h2>;
  }

  const [selectedProject] = project;

  const onClickDelete = () => {
    removeProject(selectedProject._id);
  };

  return (
    <Fragment>
      <h2>Project: {selectedProject.name}</h2>
      <ul className="listado-tareas">
        {projectTasks.length === 0 ? (
          <li className="tarea">
            <p>No pending tasks</p>
          </li>
        ) : (
          <TransitionGroup>
            {projectTasks.map(task => (
              <CSSTransition key={task._id} timeout={300} classNames="tarea">
                <Task task={task} />
              </CSSTransition>
            ))}
          </TransitionGroup>
        )}
        <button
          type="button"
          className="btn btn-eliminar"
          onClick={onClickDelete}
        >
          Remove Project &times;
        </button>
      </ul>
    </Fragment>
  );
};

export default TaskList;
