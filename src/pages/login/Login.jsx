import { useState, useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import AlertContext from "../../context/alerts/alertContext";
import AuthContext from "../../context/authentication/authContext";

const Login = (props) => {
  const alertContext = useContext(AlertContext);
  const { alert, showAlert } = alertContext;

  const authContext = useContext(AuthContext);
  const { message, authenticated, login } = authContext;

  useEffect(() => {
    if (authenticated) {
      props.history.push("/projects");
    }
    if (message) {
      showAlert(message.message, message.category);
    }
  }, [message, authenticated, props.history]);

  const [user, saveUser] = useState({
    email: "",
    password: ""
  });
  const { email, password } = user;
  const onChange = event => {
    saveUser({
      ...user,
      [event.target.name]: event.target.value,
    });
  };
  const onSubmit = event => {
    event.preventDefault();
    if (email.trim() === '' || password.trim() === '') {
      showAlert("All fields are required", "alerta-error");
      return;
    }
    login({email, password});
  };
  return (
    <div className="form-usuario">
    {alert && (
      <div className={`alerta ${alert.category}`}>{alert.message}</div>
    )}
      <div className="contenedor-form sombra-dark">
        <h1>Login</h1>
        <form onSubmit={onSubmit}>
          <div className="campo-form">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              placeholder="example@email.com"
              value={email}
              onChange={onChange}
            />
          </div>
          <div className="campo-form">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              name="password"
              placeholder="*******"
              value={password}
              onChange={onChange}
            />
          </div>
          <div className="campo-form">
            <input
              type="submit"
              className="btn btn-primario btn-block"
              value="Sing In"
            />
          </div>
        </form>
        <Link to={"/new-account"} className="enlace-cuenta">
          Create Account
        </Link>
      </div>
    </div>
  );
};

export default Login;
