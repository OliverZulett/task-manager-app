import { useContext, useEffect } from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import Project from "../../components/project/Project";
import projectContext from "../../context/projects/projectContext";
import AlertContext from "../../context/alerts/alertContext";

const ProjectList = () => {
  const projectsContext = useContext(projectContext);
  const { message, projects, getProjects } = projectsContext;

  const alertContext = useContext(AlertContext);
  const { alert, showAlert } = alertContext;

  // con useeffect puedes ejecutar cosas al principío
  useEffect(() => {
    if (message) {
      showAlert(message.message, message.category);
    }
    getProjects();
  }, [message]);

  if (projects.length === 0) return <p>No project yet, Why not create one?</p>;
  return (
    <ul className="listado-proyectos">
      {alert && (
        <div className={`alerta ${alert.category}`}>{alert.message}</div>
      )}
      <TransitionGroup>
        {projects.map((project) => (
          <CSSTransition key={project._id} timeout={200} classNames="proyecto">
            <Project project={project} />
          </CSSTransition>
        ))}
      </TransitionGroup>
    </ul>
  );
};

export default ProjectList;
