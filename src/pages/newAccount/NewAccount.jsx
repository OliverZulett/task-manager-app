import { useState, useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import AlertContext from "../../context/alerts/alertContext";
import AuthContext from "../../context/authentication/authContext";

const NewAccount = (props) => {
  const alertContext = useContext(AlertContext);
  const { alert, showAlert } = alertContext;

  const authContext = useContext(AuthContext);
  const { message, authenticated, registerUser } = authContext;

  useEffect(() => {
    if (authenticated) {
      //esto redirecciona de alguna rara forma
      props.history.push("/projects");
    }
    if (message) {
      showAlert(message.message, message.category);
    }
  }, [message, authenticated, props.history]);

  const [user, saveUser] = useState({
    name: "",
    email: "",
    password: "",
    confirm: ""
  });
  const { name, email, password, confirm } = user;
  const onChange = (event) => {
    saveUser({
      ...user,
      [event.target.name]: event.target.value
    });
  };
  const onSubmit = (event) => {
    event.preventDefault();
    if (
      name.trim() === "" ||
      email.trim() === "" ||
      password.trim() === "" ||
      confirm.trim() === ""
    ) {
      showAlert("All fields are required", "alerta-error");
      return;
    }
    if (password.length < 6) {
      showAlert("Password must have at least 6 characters", "alerta-error");
      return;
    }
    if (password !== confirm) {
      showAlert("Passwords must be equals", "alerta-error");
      return;
    }
    registerUser({
      name,
      email,
      password
    });
  };
  return (
    <div className="form-usuario">
      {alert && (
        <div className={`alerta ${alert.category}`}>{alert.message}</div>
      )}
      <div className="contenedor-form sombra-dark">
        <h1>Create Account</h1>
        <form onSubmit={onSubmit}>
          <div className="campo-form">
            <label htmlFor="name">User Name</label>
            <input
              type="text"
              id="name"
              name="name"
              placeholder="user name"
              value={name}
              onChange={onChange}
            />
          </div>
          <div className="campo-form">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              placeholder="example@email.com"
              value={email}
              onChange={onChange}
            />
          </div>
          <div className="campo-form">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              name="password"
              placeholder="*******"
              value={password}
              onChange={onChange}
            />
          </div>
          <div className="campo-form">
            <label htmlFor="confirm">Confirm Password</label>
            <input
              type="password"
              id="confirm"
              name="confirm"
              placeholder="repeat password"
              value={confirm}
              onChange={onChange}
            />
          </div>
          <div className="campo-form">
            <input
              type="submit"
              className="btn btn-primario btn-block"
              value="Sing Up"
            />
          </div>
        </form>
        <Link to={"/"} className="enlace-cuenta">
          Login
        </Link>
      </div>
    </div>
  );
};

export default NewAccount;
