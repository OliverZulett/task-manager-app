import {
  PROJECT_TASKS,
  ADD_TASK,
  VALIDATE_TASK,
  REMOVE_TASK,
  TASK_STATE,
  SELECTED_TASK,
  UPDATE_TASK,
  CLEAN_TASK
} from "../../types/index";

const TaskReducer = (state, action) => {
  switch (action.type) {
    case PROJECT_TASKS:
      // the object that is return here is the initial state in taskState
      // just send a copy and replace the things that ist needeed
      return {
        ...state,
        projectTasks: action.payload
      };
    case ADD_TASK:
      return {
        ...state,
        projectTasks: [action.payload, ...state.projectTasks],
        taskError: false
      };
    case VALIDATE_TASK:
      return {
        ...state,
        taskError: true
      };
    case REMOVE_TASK:
      return {
        ...state,
        projectTasks: state.projectTasks.filter(({ _id }) => _id !== action.payload)
      };
    case SELECTED_TASK:
      return {
        ...state,
        selectedTask: action.payload
      }
    case TASK_STATE:
    case UPDATE_TASK:
      return {
        ...state,
        projectTasks: state.projectTasks.map(
          task => (task._id === action.payload._id && action.payload) || task
        )
      }
    case CLEAN_TASK:
      return {
        ...state,
        selectedTask: null
      }
    default:
      return state;
  }
};

export default TaskReducer;
