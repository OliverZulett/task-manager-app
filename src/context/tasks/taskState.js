import React, { useReducer } from "react";
import TaskContext from "./taskContext";
import TaskReducer from "./taskReducer";
import {
  PROJECT_TASKS,
  ADD_TASK,
  VALIDATE_TASK,
  REMOVE_TASK,
  TASK_STATE,
  SELECTED_TASK,
  UPDATE_TASK,
  CLEAN_TASK
} from "../../types/index";
import axiosClient from "../../config/axios";

const TaskState = props => {
  const initialState = {
    // tasks: [],
    projectTasks: [],
    taskError: false,
    selectedTask: null
  };

  const [state, dispatch] = useReducer(TaskReducer, initialState);

  const getTasks = async projectId => {
    try {
      const resp = await axiosClient.get(`/v1/projects/${projectId}/tasks`);
      // console.log(resp);
      dispatch({
        type: PROJECT_TASKS,
        payload: resp.data.tasks
      });
    } catch (error) {
      
    }
  };

  const addTask = async task => {
    try {
      const resp = await axiosClient.post(`/v1/projects/${task.project}/tasks`, task);
      // console.log(resp);
      dispatch({
        type: ADD_TASK,
        payload: task
      });
    } catch (error) {
      // console.log(error);
      const alert = {
        message: error.response.data.message,
        category: 'alerta-error'
      }
    }
  };

  const validateTask = () => {
    dispatch({
      type: VALIDATE_TASK
    });
  };

  const removeTask = async (projectId, taskId) => {
    try {
      await axiosClient.delete(`/v1/projects/${projectId}/tasks/${taskId}`);
      dispatch({
        type: REMOVE_TASK,
        payload: taskId
      });
    } catch (error) {
      // console.log(error);
      const alert = {
        message: error.response.data.message,
        category: 'alerta-error'
      }
    }
  };

  const changeTaskState = task => {
    dispatch({
      type: TASK_STATE,
      payload: task
    })
  }

  const selectTask = task => {
    dispatch({
      type: SELECTED_TASK,
      payload: task
    })
  }

  const updateTask = async task => {
    try {
      const resp = await axiosClient.put(`/v1/projects/${task.project}/tasks/${task._id}`, task);
      dispatch({
        type: UPDATE_TASK,
        payload: resp.data
      })
    } catch (error) {
      // console.log(error);
      const alert = {
        message: error.response.data.message,
        category: 'alerta-error'
      }
    }
  }

  const cleanSelectedTask = () => {
    dispatch({
      type: CLEAN_TASK
    })
  }

  return (
    <TaskContext.Provider
      value={{
        // tasks: state.tasks,
        projectTasks: state.projectTasks,
        taskError: state.taskError,
        selectedTask: state.selectedTask,
        getTasks,
        addTask,
        validateTask,
        removeTask,
        changeTaskState,
        selectTask,
        updateTask,
        cleanSelectedTask
      }}
    >
      {props.children}
    </TaskContext.Provider>
  );
};

export default TaskState;
