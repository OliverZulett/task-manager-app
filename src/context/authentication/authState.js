import { useReducer } from "react";
import axiosClient from "../../config/axios";
import authToken from "../../config/token";
import { CLOSE_SESSION, GET_USER, LOGIN_ERROR, LOGIN_SUCCESSFUL, REGISTER_ERROR, REGISTER_SUCCESSFUL } from "../../types";
import authContext from "./authContext";
import authReducer from "./authReducer";

const AuthState = ( props ) => {
  const initialState = {
    token: localStorage.getItem("token"),
    authenticated: null,
    user: null,
    message: null,
    loading: true
  };
  const [state, dispatch] = useReducer(authReducer, initialState);

  const registerUser = async userData => {
    try {
      const resp =  await axiosClient.post('/v1/users', userData);
      // console.log(resp);
      dispatch({
        type: REGISTER_SUCCESSFUL,
        payload: resp.data
      });
      getUserAuthenticated();
    } catch (error) {
      console.error(error);
      const alert = {
        message: error.response.data.message,
        category: 'alerta-error'
      }
      dispatch({
        type: REGISTER_ERROR,
        payload: alert
      })
    }
  }

  const login = async userCredentials => {
    try {
      // console.log(userCredentials);
      const resp = await axiosClient.post('/v1/auth', userCredentials);
      dispatch({
        type: LOGIN_SUCCESSFUL,
        payload: resp.data
      });
      getUserAuthenticated();
    } catch (error) {
      console.error(error.response.data.message);
      const alert = {
        message: error.response.data.message,
        category: 'alerta-error'
      }
      dispatch({
        type: LOGIN_ERROR,
        payload: alert
      });
    }
  }

  const getUserAuthenticated = async () => {
    const token = localStorage.getItem('token');
    if (token) {
      authToken(token);
    }
    try {
      const resp = await axiosClient.get('/v1/auth/');
      dispatch({
        type: GET_USER,
        payload: resp.data.user
      })
    } catch (error) {
      // console.log(error.response);
      dispatch({
        type: LOGIN_ERROR
      })
    }
  }

  const closeSession = async () => {
    dispatch({
      type: CLOSE_SESSION
    })
  }

  return (
    <authContext.Provider
      value={{
        token: state.token,
        authenticated: state.authenticated,
        user: state.user,
        message: state.message,
        loading:  state.loading,
        registerUser,
        login,
        getUserAuthenticated,
        closeSession
      }}
    >
      {props.children}
    </authContext.Provider>
  );
};

export default AuthState;
