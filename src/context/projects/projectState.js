import { useReducer } from "react";
import projectContext from "./projectContext";
import projectReducer from "./projectReducer";
import {
  GET_PROJECTS,
  PROJECT_FORM,
  ADD_PROJECT,
  VALIDATE_FORM,
  SELECTED_PROJECT,
  REMOVE_PROJECT,
  PROJECT_ERROR
} from "../../types/index";
import axiosClient from '../../config/axios';

const ProjectState = props => {
  const initialState = {
    projects: [],
    form: false,
    errorForm: false,
    project: null,
    message: null
  };
  const showForm = () => {
    dispatch({
      type: PROJECT_FORM
    });
  };
  const getProjects = async () => {
    try {
      const resp = await axiosClient.get('/v1/projects');
      // console.log(resp);
      dispatch({
        type: GET_PROJECTS,
        payload: resp.data.projects
      });
    } catch (error) {
      // console.error(error);
      const alert = {
        message: error.response.data.message,
        category: 'alerta-error'
      }
      dispatch({
        type: PROJECT_ERROR,
        payload: alert
      });
    }
  };
  const addProject = async project => {
    try {
      const resp = await axiosClient.post('/v1/projects', project);
      // console.log(resp);
      dispatch({
        type: ADD_PROJECT,
        payload: resp.data
      });
    } catch (error) {
      console.error(error);
      const alert = {
        message: error.response.data.message,
        category: 'alerta-error'
      }
      dispatch({
        type: PROJECT_ERROR,
        payload: alert
      });
    }
  };
  const showError = () => {
    dispatch({
      type: VALIDATE_FORM
    });
  };
  const showSelectedProject = project => {
    dispatch({
      type: SELECTED_PROJECT,
      payload: project
    });
  };
  const removeProject = async projectId => {
    try {
      await axiosClient.delete(`/v1/projects/${projectId}`);
      dispatch({
        type: REMOVE_PROJECT,
        payload: projectId
      });
    } catch (error) {
      console.error(error);
      const alert = {
        message: error.response.data.message,
        category: 'alerta-error'
      }
      dispatch({
        type: PROJECT_ERROR,
        payload: alert
      });
    }
  };
  const [state, dispatch] = useReducer(projectReducer, initialState);
  return (
    <projectContext.Provider
      value={{
        projects: state.projects,
        form: state.form,
        errorForm: state.errorForm,
        project: state.project,
        message: state.message,
        showForm,
        getProjects,
        addProject,
        showError,
        showSelectedProject,
        removeProject
      }}
    >
      {props.children}
    </projectContext.Provider>
  );
};

export default ProjectState;
