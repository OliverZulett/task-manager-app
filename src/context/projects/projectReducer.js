import {
  GET_PROJECTS,
  PROJECT_FORM,
  ADD_PROJECT,
  VALIDATE_FORM,
  SELECTED_PROJECT,
  REMOVE_PROJECT,
  PROJECT_ERROR
} from "../../types";

const reducer = (state, action) => {
  switch (action.type) {
    case PROJECT_FORM:
      return {
        ...state,
        form: true
      };
    case GET_PROJECTS:
      return {
        ...state,
        projects: action.payload
      };
    case ADD_PROJECT:
      return {
        ...state,
        projects: [...state.projects, action.payload],
        form: false,
        errorForm: false
      };
    case VALIDATE_FORM:
      return {
        ...state,
        errorForm: true
      };
    case SELECTED_PROJECT:
      return {
        ...state,
        project: state.projects.filter(({ _id }) => _id === action.payload._id)
      };
    case REMOVE_PROJECT:
      return {
        ...state,
        projects: state.projects.filter(({ _id }) => _id !== action.payload),
        project: null
      };
    case PROJECT_ERROR:
      return {
        ...state,
        message: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
