import { useReducer } from "react";
import { DIMISS_ALERT, SHOW_ALERT } from "../../types";
import alertContext from "./alertContext";
import alertReducer from "./alertReducer";

const AlertState = (props) => {
  const initialState = {
    alert: null
  };
  const [state, dispatch] = useReducer(alertReducer, initialState);

  const showAlert = (message, category) => {
    dispatch({
      type: SHOW_ALERT,
      payload: {
        message,
        category
      }
    });
    setTimeout(() => {
      dispatch({
        type: DIMISS_ALERT
      });
    }, 5000);
  };

  return (
    <alertContext.Provider
      value={{
        alert: state.alert,
        showAlert
      }}
    >
      {props.children}
    </alertContext.Provider>
  );
};

export default AlertState;
